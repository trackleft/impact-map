var map;
var baselayers = ["counties", "legislative0", "legislative1", "congress"];
var uaLocationLayers = ["news", "academic", "campus", "extension", "agcenters", "sites", "telemedicine"];
var checkedBoxes = {};

require([
    "esri/map",
    "esri/layers/FeatureLayer",
    "esri/layers/GraphicsLayer",
    "esri/layers/WebTiledLayer",
    "extras/ClusterLayer",
    "extras/geojsonlayer",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/SpatialReference",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/PictureMarkerSymbol",
    "esri/renderers/SimpleRenderer",
    "esri/renderers/ClassBreaksRenderer",
    "esri/Color",
    "esri/dijit/Popup",
    "esri/dijit/PopupTemplate",
    "dojo/dom-construct",
    "dojo/domReady!"
    ], function(
        Map,
        FeatureLayer,
        GraphicsLayer,
        WebTiledLayer,
        ClusterLayer,
        GeoJsonLayer,
        Point,
        webMercatorUtils,
        SpatialReference,
        SimpleLineSymbol,
        SimpleFillSymbol,
        SimpleMarkerSymbol,
        PictureMarkerSymbol,
        SimpleRenderer,
        ClassBreaksRenderer,
        Color,
        Popup,
        PopupTemplate,
        domConstruct
        ) {
    /////////////////// LOCAL VARIABLES ////////////////////////////////////////////////////////////////
    //var url = "http://apps.cals.arizona.edu/gis/rest/services/ImpactMap/MapServer/";
    var url = "http://services.maps.arizona.edu/pdc/rest/services/ImpactMap2014/MapServer/";
    var outFieldsUA =  ["Name","Address","City","State","Zip"];
    var maxOff = 3000;

    //background of Arizona. Represents the un-highlighted county/district fill color.
    var sfs = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
              new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
              new Color([45, 80, 225, 0.0]), 1), //line color, transparancy, width
              new Color([45, 80, 225, .25]));     //fill color, transparancy
    var rendererBackground = new SimpleRenderer(sfs);

    //un-highlighted county/district fill color.
    var sfs2 = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
               new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
               new Color([123, 137, 174, 1]), .6),//line color, transparancy, width
               new Color([123, 137, 174,0 ]));   //fill color, transparancy
    var rendererUnselected = new SimpleRenderer(sfs2);

    //highlighted county/district fill color.
    var sfs3 = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
               new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
               new Color([39, 101, 232, 1]), .6),
               new Color([39, 101, 232, .252]));
    var rendererSelected = new SimpleRenderer(sfs3);

    var popup = new Popup({
        anchor:"top",
        highlight: false,
        zoomFactor: 4,
        pagingInfo: true,
        titleInBody: false
    }, domConstruct.create("div"));

    var popups = [];
    for (i = 0; i < uaLocationLayers.length; i++) {
        popups.push(
            new PopupTemplate({
                title:$("label[for='" + uaLocationLayers[i] + "']").text().trim(),
                description: "{Name}<br>{Address}<br>{City} {State} {Zip}"
            })
        );
    }

    var templateNews = new PopupTemplate({
        title: "UANews",
        description: "<a href='{Path}'>{name}</a>"
    });
    //////////////////////////////////////////////////////////////////////////////////////////////////
    var initExtent = {};
    if ($.bbq.getState("x") === undefined || $.bbq.getState("y") === undefined) {
        initExtent.x = -111.93488020312306;
        initExtent.y = 34.220817631763204;
    } else {
        initExtent.x = $.bbq.getState("x");
        initExtent.y = $.bbq.getState("y");
    }

    map = new Map("map-canvas", {
        center: [initExtent.x, initExtent.y],
        zoom: ($.bbq.getState("z") === undefined) ? 7 : $.bbq.getState("z"),
        minZoom: 6,
        maxZoom: 18,
        "logo": false,
        infoWindow: popup,
        smartNavigation: false
    });

    var subDomains = ["a", "b", "c", "d"];
    var mapBoxLayer = new WebTiledLayer("http://${subDomain}.tiles.mapbox.com/v4/universityrelations.jom2cjn0/${level}/${col}/${row}.png?access_token=pk.eyJ1IjoidW5pdmVyc2l0eXJlbGF0aW9ucyIsImEiOiJkcEN3N1lRIn0.ipOYX9wi9uAGI79kIiAOFg", {
        "subDomains": subDomains,
        "id": "mapBoxLayer"
    });
    map.addLayer(mapBoxLayer);

    //LAYER DEFINITIONS
    ////////////////BASELAYERS///////////////
    var azmouseover = new FeatureLayer(url + 11, {
        id: "azmouseover",
        mode: FeatureLayer.MODE_SNAPSHOT,
        //maxAllowableOffset: maxOff,
        visible: true,
        outFields: ["Governor", "Attorney_General", "Secretary_State", "Students",
            "Alumni", "Employees", "StudentsFinancialAid", "Degrees", "Economic_Impact"]
    });
    map.addLayer(azmouseover);

    var azbackground = new FeatureLayer(url + 10, {
        id: "azbackground",
        mode: FeatureLayer.MODE_SNAPSHOT,
        //maxAllowableOffset: maxOff,
        visible: true,
        //outFields: ["*"]
    });
    azbackground.setRenderer(rendererBackground);
    map.addLayer(azbackground);

    var counties = new FeatureLayer(url + 9, {
        id: baselayers[0],
        mode: FeatureLayer.MODE_ONDEMAND,
        maxAllowableOffset: maxOff,
        visible: ($.bbq.getState("layer") === baselayers[0]),
        outFields: ["*"]
    });
    counties.setRenderer(rendererUnselected);
    map.addLayer(counties);

    var legislative0 = new FeatureLayer(url + 6, {
        id: baselayers[1],
        mode: FeatureLayer.MODE_ONDEMAND,
        maxAllowableOffset: maxOff,
        visible: ($.bbq.getState("layer") === baselayers[2]),
        outFields: ["*"]
    });
    legislative0.setRenderer(rendererUnselected);
    map.addLayer(legislative0);

    var legislative1 = new FeatureLayer(url + 7, {
        id: baselayers[2],
        mode: FeatureLayer.MODE_ONDEMAND,
        maxAllowableOffset: maxOff,
        visible: ($.bbq.getState("layer") === baselayers[2]),
        outFields: ["*"]
    });
    legislative1.setRenderer(rendererUnselected);
    map.addLayer(legislative1);

    var congress = new FeatureLayer(url + 8, {
        id: baselayers[3],
        mode: FeatureLayer.MODE_ONDEMAND,
        maxAllowableOffset: maxOff,
        visible: ($.bbq.getState("layer") === baselayers[3]),
        outFields: ["*"]
    });
    congress.setRenderer(rendererUnselected);
    map.addLayer(congress);

    var selectedFeatures = new GraphicsLayer({
        id: "selectedFeatures"
    });
    selectedFeatures.setRenderer(rendererSelected);
    map.addLayer(selectedFeatures);


    ////////////OPERATIONAL LAYERS///////////////
    var uanewsGeoJSON = new GeoJsonLayer({
        visible: false, //will show on the ClusterLayer
        url: "http://uanews.org/feed/geojson/impact-map-news", //CORS must be enabled
        id: "uanewsGeoJSON"
    });
    map.addLayer(uanewsGeoJSON);

    var telemedicine = new FeatureLayer(url + 5, {
        id: uaLocationLayers[6],
        mode: FeatureLayer.MODE_SNAPSHOT,
        visible: ($.bbq.getState(uaLocationLayers[6]) !== "false"),
        infoTemplate: popups[6],
        outFields: outFieldsUA
    });
    telemedicine.renderer = getRenderer(uaLocationLayers[6], 28, 42);
    map.addLayer(telemedicine);

    var sites = new FeatureLayer(url + 4, {
        id: uaLocationLayers[5],
        mode: FeatureLayer.MODE_SNAPSHOT,
        visible: ($.bbq.getState(uaLocationLayers[5]) !== "false"),
        infoTemplate: popups[5],
        outFields: outFieldsUA
    });
    sites.renderer = getRenderer(uaLocationLayers[5], 28, 42);
    map.addLayer(sites);

    var extension = new FeatureLayer(url + 3, {
        id: uaLocationLayers[3],
        mode: FeatureLayer.MODE_SNAPSHOT,
        visible: ($.bbq.getState(uaLocationLayers[3]) !== "false"),
        infoTemplate: popups[3],
        outFields: ["Name","Address","City","State","Zip", "Phone"]
    });
    extension.renderer = getRenderer(uaLocationLayers[3], 28, 42);
    map.addLayer(extension);

    var agcenters = new FeatureLayer(url + 2, {
        id: uaLocationLayers[4],
        mode: FeatureLayer.MODE_SNAPSHOT,
        visible: ($.bbq.getState(uaLocationLayers[4]) !== "false"),
        infoTemplate: popups[4],
        outFields: outFieldsUA
    });
    agcenters.renderer = getRenderer(uaLocationLayers[4], 28, 42);
    map.addLayer(agcenters);

    var academic = new FeatureLayer(url + 0, {
        id: uaLocationLayers[1],
        mode: FeatureLayer.MODE_SNAPSHOT,
        visible: ($.bbq.getState(uaLocationLayers[1]) !== "false"),
        infoTemplate: popups[1],
        outFields: outFieldsUA
    });
    academic.renderer = getRenderer(uaLocationLayers[1], 28, 42);
    map.addLayer(academic);

    var campus = new FeatureLayer(url + 1, {
        id: uaLocationLayers[2],
        mode: FeatureLayer.MODE_SNAPSHOT,
        visible: true,
        infoTemplate: popups[2],
        outFields: outFieldsUA
    });
    campus.renderer = getRenderer(uaLocationLayers[2], 27, 25);
    map.addLayer(campus);

    //load clusters from GeoJSON layer
    uanewsGeoJSON.on("update-end", function(evt) {
        var layer = evt.target;

        var data = dojo.map(layer.graphics, function(graphic) {
            var latlng = new Point(parseFloat(graphic.geometry.x), parseFloat(graphic.geometry.y), new SpatialReference({
                "wkid": 4326
            }));
            var webMercator = webMercatorUtils.geographicToWebMercator(latlng);
            return {
                "x": webMercator.x,
                "y": webMercator.y,
                "attributes": graphic.attributes
            };
        });
        addClusterLayer(data);

        //bring campus to the top of the order.
        map.reorderLayer(campus, 100);
        //add news items to the more impact report, on init
        updateNewsOnImpactReport(null);
    });

    function addClusterLayer(data) {
        var news = new ClusterLayer({
            "data": data,
            "distance": 100,
            "id": "news",
            "visible" : ($.bbq.getState(uaLocationLayers[0]) !== "false"),
            "labelColor": "#fff",
            "labelOffset": 10,
            "resolution": map.extent.getWidth() / map.width,
            "showSingles": false,
            "singleTemplate": templateNews
        });
        var defaultSym = new SimpleMarkerSymbol().setSize(4);
        var rendererCluster = new ClassBreaksRenderer(defaultSym, "clusterCount");

        //var newsSymbol = getRenderer(uaLocationLayers[0], 28, 42);
        var newsSymbol = new PictureMarkerSymbol("markers/news.svg", 28, 42).setOffset(0, 8);
        rendererCluster.addBreak(0, 1000, newsSymbol);

        news.setRenderer(rendererCluster);
        map.addLayer(news);

        //add event
        news.on("click", function(evt) {
            if(map.infoWindow.isShowing) {
                //modify css class for info Window.
                $(".esriPopup").removeClass(uaLocationLayers.toString().replace(/,/g, " "));
                $(".esriPopup").addClass("news");
            }
        });
    }

    ////////////////// EVENT /////////////////////
    //make sure state is in viewport on load
    azbackground.on("load", function(evt) {
        if ($.bbq.getState("z") === undefined)
            map.setExtent(evt.layer.fullExtent, true);
    })

    map.on("click", function(evt) {
        unlockStats(evt.graphic);
    });
    map.on("pan-end", shareMapExtent);

    map.on("zoom-end", function(extent) {
        $.bbq.pushState({z:map.getZoom()});
    });

    //load extension offices on more impact report, on init
    extension.on("update-end", function(evt) {
        updateExtensionOfficesOnImpactReport(map.getLayer("extension").graphics);
    });

    //mouse-out of Arizona to show statewide statistics
    //load stats on init
    map.getLayer("azmouseover").on("update-end", function(evt) {
        updateStats(evt.target.graphics[0].attributes);
    });

    map.getLayer("azmouseover").on("mouse-over", function(evt) {
        updateStats(evt.graphic.attributes);
    });

    map.getLayer("azmouseover").on("click", function(evt) {
        updateStats(evt.graphic.attributes);
    });

    //clicks, mouse-overs - counties & districts
    for (i = 0; i < baselayers.length; i++) {
        map.getLayer(baselayers[i]).on("mouse-over", function(evt) {
            updateStats(evt.graphic.attributes);
        });

        map.getLayer(baselayers[i]).on("click", function(evt) {
            lockStats(evt.graphic);

        });
    }

    //click events for UA Locations
    for (i = 1; i < uaLocationLayers.length; i++) {
        map.getLayer(uaLocationLayers[i]).on("click", function(evt) {
            if(map.infoWindow.isShowing) {
                //need to close the infoWindow to trigger the on."show" event.
                //the on."show" event has code to modify the css classes.
                map.infoWindow.hide();
            }
        });
    }

    // close the info window when esc is pressed
    map.on("key-down", function(e) {
        if (e.keyCode === 27) {
            map.infoWindow.hide();
        }
    });

    map.infoWindow.on("show", function(evt) {
        //replaces commas with spaces, which jQuery needs to .removeClass("multiple" "classes")
        $(".esriPopup").removeClass(uaLocationLayers.toString().replace(/,/g, " "));

        //sometimes the evt.target.features is not passed in the event. If this is the case, it's news.
        //console.warn(evt.target.features);
        if(evt.target.features === undefined || evt.target.features === null || evt.target.features.length == 0) { //news layer does not have a "features" property, the rest do.
            $(".esriPopup").addClass("news");
        }
        else {
            $(".esriPopup").addClass(evt.target.features[0]._layer.id);
        }
        //console.log($(".esriPopup").attr("class"));
    });

    map.infoWindow.on("selection-change", function(evt) {
        if(map.infoWindow.isShowing) {
            try {
                // console.warn(evt);
                // console.log(evt.target.selectedIndex);
                // console.log(evt.target.features[evt.target.selectedIndex]._layer.id);
                $(".esriPopup").removeClass(uaLocationLayers.toString().replace(/,/g, " "));
                $(".esriPopup").addClass(map.infoWindow.getSelectedFeature()._layer.id);
                //console.log($(".esriPopup").attr("class"));
            }
            catch (err) {
                //console.warn("no target");
            }
        }
    });
});

function clipUALocation(graphic) {
    //NOT BEING USED. TRIED TO HIDE GRAPHICS AS THEY WERE BEING ADDED TO THE MAP BY HOOKING INTO .on("graphic-draw") event.
    if (map.getLayer("selectedFeatures").graphics.length === 0) {
        console.debug("selectedFeatures is not present");
        return;
    } else {
        var polyGeometry = map.getLayer("selectedFeatures").graphics[0].geometry;
        if (!polyGeometry.contains(graphic.geometry)) {
            //console.debug("hiding!");
            graphic.hide();
        }
    }
}

function clipUALocations() {
    if (map.getLayer("selectedFeatures").graphics.length === 0) {
        console.debug("selectedFeatures is not present");
        return;
    } else {
        //hide graphics that are outside selected polygon

        //get Geometry for selected polygon
        var polyGeometry = map.getLayer("selectedFeatures").graphics[0].geometry;
        var extensionOffices = [];

        //loop through each UA Location
        for (i = 0; i < uaLocationLayers.length; i++) {
            var uaLayer = map.getLayer(uaLocationLayers[i]);

            if (uaLayer.visible) {
                //console.warn(uaLocationLayers[i]);
                //console.debug("TOTAL: " + uaLayer.graphics.length);

                //loop through each Graphic
                for (j = 0; j < uaLayer.graphics.length; j++) {
                    //if the geometry of the UALayer's graphic is outside the geometry of the selected polygon, hide it.
                    if (!polyGeometry.contains(uaLayer.graphics[j].geometry)) {
                        //console.debug("hiding");
                        uaLayer.graphics[j].hide();
                    }
                    //else console.debug("showing");
                }
            }

            //put the clipped extension locations on the More Impact Report
            if (uaLocationLayers[i] === "extension") {
                for (j = 0; j < uaLayer.graphics.length; j++) {
                    if (polyGeometry.contains(uaLayer.graphics[j].geometry)) {
                        extensionOffices.push(uaLayer.graphics[j]);
                    }
                }
            }
        }
    updateExtensionOfficesOnImpactReport(extensionOffices);
    updateNewsOnImpactReport(polyGeometry);
    }
}

function unclipUALocations() {
    //show all graphics for visible UA Location Layers
    for (i = 0; i < uaLocationLayers.length; i++) {
        var uaLayer = map.getLayer(uaLocationLayers[i]);
        if (uaLayer.visible) {
            for (j = 0; j < uaLayer.graphics.length; j++) {
                uaLayer.graphics[j].show();
            }
        }
    }
    updateExtensionOfficesOnImpactReport(map.getLayer("extension").graphics);
    updateNewsOnImpactReport(null);
    $.bbq.pushState({region: "unselected"});
}

function changeBaseLayer(layer) {
    //clear any selections and unlockStats
    $.bbq.pushState({layer: layer});
    $('.current-layer').removeClass('current-layer');
    $('#' + layer).addClass('current-layer');

    //clear selected features.
    if (map.getLayer("selectedFeatures").graphics.length > 0) {
        unclipUALocations();
        map.getLayer("selectedFeatures").clear();
    }
    //hide all baselayers
    for (i = 0; i < baselayers.length; i++) {
        map.getLayer(baselayers[i]).hide();
    }
    //show selected baselayer
    map.getLayer(layer).show();
    if (layer === "legislative1") { //add both leg1 and 0
        map.getLayer("legislative0").show();
    }

    //reenable the mouse events...
    map.getLayer(layer).enableMouseEvents();
}

function toggleLayer(layer) {
    var bs = (map.getLayer(layer).visible) ? hideLayer(layer) : showLayer(layer);
}

function showLayer(layer) {
    map.getLayer(layer).show();
    //var bool = $('#' + layer).prop('checked');
    checkedBoxes[layer] = true;
    $.each(checkedBoxes, function(key, value) {
        $.bbq.pushState(checkedBoxes);
    });
}

function hideLayer(layer) {
    map.getLayer(layer).hide();
    //var bool = $('#' + layer).prop('checked');
    checkedBoxes[layer] = false;
    $.each(checkedBoxes, function(key, value) {
        $.bbq.pushState(checkedBoxes);
    });
}

function shareMapExtent() {
    var wm = esri.geometry.webMercatorToGeographic(map.extent.getCenter());
    $.bbq.pushState({x:wm.x});
    $.bbq.pushState({y:wm.y});
    //$.bbq.pushState({z:map.getZoom()});
}

function updateStats(stats) {
//    console.log(stats);
    if (stats.Name != undefined) { //county
        $(".data-name").html(stats.Name + " County");
        $(".data-economic").removeClass('hidden').html('$ ' + commaFormat(stats.Economic_Impact));
        $(".data-economic-short").removeClass('hidden').html('$ ' + format_number(stats.Economic_Impact));
        $(".label-economic").removeClass('hidden');
        $(".name-container").html('<h3 role="alert" class="data-name fadeOutUp animated">' + stats.Name + '</h3>');

        //elected officials
        electedArray = stats["Supervisor"].split("|");
        var electedHTML = "";
        for (var i=0; i < electedArray.length; i++) {
            electedHTML = electedHTML
                + "<tr>"
                + "     <td class='table-label'>District " + (i+1) + "</td>"
                + "     <td class='table-data'>" + electedArray[i] + "</td>"
                + "</tr>";
        }

    } else if (stats.District != undefined) { //district
        $(".data-economic").addClass('hidden').html("N/A");
        $(".data-economic-short").addClass('hidden').html("N/A");
        $(".label-economic").addClass('hidden');
        $(".name-container").html('<h3 role="alert" class="data-name fadeOutUp animated">District ' + stats.District + '</h3>');

        if (stats.Senator != undefined) { //state district
            $(".data-name").html("District " + stats.District.replace(/^0+/, '')); //removes preceding 0
            var electedHTML = ""
                + "<tr>"
                + "     <td class='table-label'>Senator</td>"
                + "     <td class='table-data'>" + stats.Senator + "</td>"
                + "</tr>"
                + "<tr>"
                + "     <td class='table-label'>Representative</td>"
                + "     <td class='table-data'>" + stats.Rep1 + "</td>"
                + "</tr>"
                + "<tr>"
                + "     <td class='table-label'>Representative</td>"
                + "     <td class='table-data'>" + stats.Rep2 + "</td>"
                + "</tr>"
                ;
            }
        else{ //congressional district
            $(".data-name").html("District " + stats.District);
                var electedHTML = ""
                + "<tr>"
                + "     <td class='table-label'>Representative</td>"
                + "     <td class='table-data'>" + stats.Representative + "</td>"
                + "</tr>"
                ;
        }

    } else { //statewide stats
        $(".data-name").html("Arizona Impact");
        $(".name-container").html('<h3 role="alert" class="data-name fadeOutUp animated">Arizona</h3>');
        $(".data-economic").removeClass('hidden').html('$ ' + commaFormat(stats.Economic_Impact));
        $(".data-economic-short").removeClass('hidden').html('$ ' + format_number(stats.Economic_Impact));
        $(".label-economic").removeClass('hidden');
        var electedHTML = ""
            + "<tr>"
            + "     <td class='table-label'>Governor</td>"
            + "     <td class='table-data'>" + stats.Governor + "</td>"
            + "</tr>"
            + "<tr>"
            + "     <td class='table-label'>Secretary of State</td>"
            + "     <td class='table-data'>" + stats.Secretary_State + "</td>"
            + "</tr>"
            + "<tr>"
            + "     <td class='table-label'>Attorney General</td>"
            + "     <td class='table-data'>" + stats.Attorney_General + "</td>"
            + "</tr>"
            ;
    }

    $(".data-students").html(commaFormat(stats.Students));
    $(".data-alumni").html(commaFormat(stats.Alumni));
    $(".data-employees").html(commaFormat(stats.Employees));
    $(".data-financialaid").html('$ ' + commaFormat(stats.StudentsFinancialAid));
    $(".data-financialaid-short").html('$ ' + format_number(stats.StudentsFinancialAid));
    $(".data-degrees").html(commaFormat(stats.Degrees));
    $("#data-officials").html(electedHTML);
}

function updateNewsOnImpactReport(polyGeometry) {
    var newsItems = [];
    var newsLayer = map.getLayer("uanewsGeoJSON");

    //no clipping. Show all news items
    if(polyGeometry == null) {
        newsItems = newsLayer.graphics;
    }
    else {  //some geometry was passed
        for (j = 0; j < newsLayer.graphics.length; j++) {
            if (polyGeometry.contains(newsLayer.graphics[j].geometry)) {
                newsItems.push(newsLayer.graphics[j]);
            }
        }
    }

    var newsItemsHTML = "";
    var arr = {};
    for ( var i=0; i < newsItems.length; i++ )
        arr[newsItems[i].attributes.UUID] = newsItems[i];
    newsItems = new Array();
    for ( var key in arr ){
        newsItems.push(arr[key]);
    }
    for (var i=0; i < newsItems.length; i++) {
        newsItemsHTML = newsItemsHTML
            + '<article class="row no-margin card">'
            + '  <a class="article clearfix" href="' + newsItems[i].attributes.Path + '" style="display:block;" target="_blank">'
            + '     <div class="col-xs-12 l-no-gutter">'
            +        newsItems[i].attributes.image
            + '     </div>'
            + '     <div class="col-xs-12">'
            + '         <h5 class="first">' + newsItems[i].attributes.name + '</h5>'
            //+ '         <p class="summary">' + newsItems[i].attributes["Short Summary"] + ' <a class="read-more" href=' + newsItems.attributes.Path +'>Read More</span>'
            + '         <p class="summary">' + newsItems[i].attributes["Short Summary"] + ' <span class="read-more">READ MORE</span>'
            + '         </p>'
            + '     </div>'
            + ' </a>'
            + '</article>'
            ;
    }
    $("#news-articles").html(newsItemsHTML);
}

function updateExtensionOfficesOnImpactReport(extensionOffices) {
        var extensionHTML = "";
        for (var i=0; i < extensionOffices.length; i++) {
            extensionHTML = extensionHTML
                + '<tr id="office-first">'
                + '     <td id="data-extOffice">' + extensionOffices[i].attributes.Name + '</td>'
                + '</tr>'
                + '<tr>'
                + '     <td id="data-extAddress">' + extensionOffices[i].attributes.Address + '</td>'
                + '</tr>'
                + '<tr>'
                + '     <td id="data-extAddress2">' + extensionOffices[i].attributes.City + ', '
                                                   + extensionOffices[i].attributes.State + ' '
                                                   + extensionOffices[i].attributes.Zip + '</td>'
                + '</tr>'
                + '<tr id="office-last">'
                + '     <td id="data-extPhone"><span itemprop="telephone"><a href="tel:+1' + extensionOffices[i].attributes.Phone +  '">' + extensionOffices[i].attributes.Phone + '</a></span></td>'
                + '</tr>'
                ;
        }
        $("#extension-offices").html(extensionHTML);
}

function lockStats(graphic) {
    if(graphic === null) {  //lockStats called from ShareURL
        var baseLayer = map.getLayer($.bbq.getState("layer"));  //determine baselayer, "counties", "legislative", "congressional"

        //loop trhough graphics
        for(i=0;i<baseLayer.graphics.length;i++) {
            if(baseLayer.graphics[i].attributes.Name === $.bbq.getState("region") || baseLayer.graphics[i].attributes.District === $.bbq.getState("region")) {
                graphic = baseLayer.graphics[i];
                break;
            }
        }
        if(graphic===null) {
            console.warn("uh oh, selected graphic = null;");
            return;
        }
    }
    else {  //update share URL
        if(graphic.attributes.Name === undefined) //District Map
            $.bbq.pushState({region: graphic.attributes.District});
        else //County
            $.bbq.pushState({region: graphic.attributes.Name});
    }

    //clear map and disable mouse-over events.
    map.getLayer("selectedFeatures").clear();
    disableLayersMouseOver();

    //shade selected polygon
    map.getLayer("selectedFeatures").add(new esri.Graphic(graphic.geometry));

    //update stats
    updateStats(graphic.attributes);

    //clip UA Locations to selected polygon
    clipUALocations();
}

function unlockStats(graphic) {
    if (graphic === undefined) { //no graphics were selected on mouse click...
        map.getLayer("selectedFeatures").clear();
        enableLayersMouseOver();
        unclipUALocations();
    }
}

function enableLayersMouseOver() {
    map.getLayer("azmouseover").enableMouseEvents();
    for (i = 0; i < baselayers.length; i++) {
        map.getLayer(baselayers[i]).enableMouseEvents();
    }
}

function disableLayersMouseOver() {
    map.getLayer("azmouseover").disableMouseEvents();
    for (i = 0; i < baselayers.length; i++) {
        map.getLayer(baselayers[i]).disableMouseEvents();
    }
}

function getRenderer(layer, width, height) {
    var renderer;
    require(["esri/symbols/PictureMarkerSymbol", "esri/renderers/SimpleRenderer"],
        function(PictureMarkerSymbol, SimpleRenderer) {
            var pictureMarkerSymbol = new PictureMarkerSymbol("markers/" + layer + ".svg", width, height).setOffset(0,21);
            renderer = new SimpleRenderer(pictureMarkerSymbol);
        });
    return renderer;
}

function ShowExtents() {
    // show cluster extents.. never called directly but useful from the console
    require([
        "esri/layers/GraphicsLayer", "esri/geometry/Extent",
        "esri/symbols/SimpleFillSymbol", "esri/Color","esri/graphic"
        ],
        function(GraphicsLayer, Extent, SimpleFillSymbol, Color, Graphic) {
            ClearExtents();
            var extents = new GraphicsLayer({
                id: "clusterExtents"
            });
            var sym = new SimpleFillSymbol().setColor(new Color([205, 193, 197, 0.5]));

            dojo.forEach(map.getLayer("news")._clusters, function(c, idx) {
                var e = c.attributes.extent;
                extents.add(new Graphic(new Extent(e[0], e[1], e[2], e[3], map.spatialReference), sym));
            }, this);
            map.addLayer(extents, 0);
        });
}

function ClearExtents() {
    // clear cluster extents..
    var extents = map.getLayer("clusterExtents");
    if (extents) {
        map.removeLayer(extents);
    }
}

function commaFormat(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

function addPoint(number) {
    number += '';
    x = number.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{2})/;
    x1 = x1.replace(rgx, '$1' + '.' + '$2');
    return x1 + x2;
}

function format_number(number) {
    // strip any commas
    //number = parseFloat(number.replace(/,/g, ''));
    number = Number(number);
    // make sure it's a number...
    if (isNaN(number)) {
        return false;
        i
    }
    // filter and format it
    if (number >= 1000000000000) {
        number = Math.round((number / 10000000000), -2);
        number = addPoint(number) + ' tr';
        return number;
    } else if (number >= 1000000000) {
        number = Math.round((number / 10000000), -2);
        number = addPoint(number) + ' bn';
        return number;
    } else if (number >= 1000000) {
        number = Math.round((number / 10000), -2);
        number = addPoint(number) + ' MM';
        return number;
    }
    return number.toFixed();
}

window.onload = function() {
    //After all graphics have been loaded, check share url for previously selected county/district
    if(!($.bbq.getState("region") === undefined || $.bbq.getState("region") === "unselected")) {
        lockStats(null);
    }

    $('#map-canvas').css({'height':$(window).height()+'px'});
    $('.spacer').css({'height':$(window).height()+'px'});
    $('.overmap-content-map').css({'height':$(window).height()+'px'});
};
